//this is a project that has endpoints and is an api this wll be deployed on gitlab using ci/cd pipelines
const express = require("express");
const app = express();
const port = 5000;

//endpoint
app.get('/',(req,res)=>{
    res.send("This is the endpoint.");
})

app.listen(port ,()=>{
    console.log(`App listening at http://localhost:${port}`);
})